package it.unibo.coordination.linda.cli

enum class TupleSpaceTypes {
    LOGIC,
    TEXT
}