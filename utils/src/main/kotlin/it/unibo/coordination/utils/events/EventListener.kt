package it.unibo.coordination.utils.events

typealias EventListener<Arg> = (Arg)->Any